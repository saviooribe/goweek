import React, {component} from 'react';
import './styles.css';
import { Link } from 'react-router-dom';
import { Button,Container, Row, Col } from 'reactstrap';


export default class Main extends React.Component{
    

    render(){
        return (
            <Container className='product-list'>
                <Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <div>
                            <Button color="success" >
                                { <Link to='/clientes'> clientes </Link>                                      
                            
                                }</Button>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <div>
                            <Button color="success" >
                                { <Link to='/usuarios'> usuarios </Link>                                      
                            
                                }</Button>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}