import React, {component} from 'react';
import api from '../../services/api';

import './styles.css'

export default class Product extends React.Component{
    state = {
        cliente:{},
    };

    async componentDidMount(){
        const { id } = this.props.match.params;

        const response = await api.get(`/clientes/ver/${id}`);

        this.setState({ cliente: response.data.cliente });
        
    }

    render(){
        const { cliente } =this.state;
        console.log(cliente)
        return (
            <div className="cliente-info">
                <h1>{cliente.name}</h1>
                <p>{cliente.endereco}</p>
            </div>
            
        )
    }
}