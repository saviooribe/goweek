import React, {component} from 'react';
import api from '../../services/api';
import { Link, Redirect } from 'react-router-dom';
import './styles.css';


export default class Clientes extends React.Component{
    state = {
        user:[],
        
    }
    
    
    componentDidMount(){
        this.loadClientes();
    }
    loadClientes = async () => {
        const response = await api.get('/user/user')
        this.setState({user: response.data.user })
        
    }
    
    

    render(){
        const { user } = this.state;
        console.log(user)
        return (
            <div className='user-list'>
            <Link to={'/user/adicionar'}>Adicionar</Link>
                {user.map(user =>(
                    
                    <article key={user._id}>
                        <strong>{user.name}</strong>
                        <p>{user.email}</p>


                        <Link to={`/user/delete/${user._id}`}>Deletar</Link>
                        <Link to={`/user/editar/${user._id}`}>Editar</Link>
                        <Link to={`/user/ver/${user._id}`}>Ver +</Link>
                    </article>
                ))}
                    <div className='actions'>
                        
                    </div> 
            </div>
        )
    }
}