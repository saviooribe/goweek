import React, { Component } from 'react';
import './styles.css';
import api from '../../services/api';
var request = require('request');



export default class Login extends React.Component {
    state ={
        email:'',
        senha:'',
        token:'',
        
    };
    handleSubmit = async e =>{
            e.preventDefault();
        
            const { email, senha}= this.state;
            
            if(!email.length) return;        
            
        const response  = await api.post('auth/login', { email, senha});

        this.setState({token: response.data.token})

        const token = (response.data.token);
       
    await this.props.history.push('/main');

    };
    handleLogin = async e =>{
        const response = await api.get(`/registro`)
   await this.props.history.push('/main');
    };

    handleInputChange = async (name, e) => {
        this.setState({
        [name]: e.target.value
        });
    }

  render() {
    return (
        <div className='login-wrapper'>
        <h2>Faça seu registro</h2>
        <form onSubmit={this.handleSubmit} >
            <input
                name='email'
                onChange={e =>this.handleInputChange('email', e)}
                value={this.state.email} 
                placeholder='E-mail'
            />
            <input
                name='senha'
                type='password'
                onChange={e =>this.handleInputChange('senha', e)}
                value={this.state.senha} 
                placeholder='Senha'
            />
            <button onChange={this.handleSubmit} type='submit'>Entrar</button>
            <button to='/registro' >Registra-se</button>
            
        </form>
        </div>
    );
  }
}