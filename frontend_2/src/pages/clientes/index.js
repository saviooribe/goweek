import React, {component} from 'react';
import api from '../../services/api';
import { Link, Redirect } from 'react-router-dom';
import './styles.css';


export default class Clientes extends React.Component{
    state = {
        clientes:[],
        
    }
    
    
    componentDidMount(){
        this.loadClientes();
    }
    loadClientes = async () => {
        const response = await api.get('/clientes/clientes')
        this.setState({clientes: response.data.clientes })
        
    }
    
    

    render(){
        const { clientes } = this.state;
        console.log(clientes)
        return (
            <div className='cliente-list'>
            <Link to={'/clientes/adicionar'}>Adicionar</Link>
                {clientes.map(cliente =>(
                    
                    <article key={cliente._id}>
                        <strong>{cliente.name}</strong>
                        <p>{cliente.endereco}</p>


                        <Link to={`/clientes/delete/${cliente._id}`}>Deletar</Link>
                        <Link to={`/clientes/editar/${cliente._id}`}>Editar</Link>
                        <Link to={`/clientes/ver/${cliente._id}`}>Ver +</Link>
                    </article>
                ))}
                    <div className='actions'>
                        
                    </div> 
            </div>
        )
    }
}