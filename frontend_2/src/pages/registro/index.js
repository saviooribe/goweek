import React, { Component } from 'react';
import './styles.css';
import api from '../../services/api';
import { Link } from 'react-router-dom';


export default class Registro extends React.Component {
    state ={
        name:'',
        email:'',
        senha:'',
        token:'',
        
    };
    handleSubmit = async e =>{
        e.preventDefault();
    
        const {name, email, senha}= this.state;
        
        if(!email.length) return;

        const token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjNTQ1NzUxMTEyNmQ0MTg4ZTEzNDkyMyIsImlhdCI6MTU0OTAzOTAxNCwiZXhwIjoxNTQ5MTI1NDE0fQ.VSNAXd_SvwIsn6JW9UXyrC20asSdy6Hux4bASEaP7vY'
        
        
        console.log({name, email, senha})
    await api.post('auth/registro', {name, email, senha})
    token.setRequestHeader('autorization', token);
   await this.props.history.push('/main');
    };
    handleLogin = async e =>{
        const response = await api.get(`/login`)
   await this.props.history.push('/main');
    };

    handleInputChange = async (name, e) => {
        this.setState({
        [name]: e.target.value
        });
    }

  render() {
    return (
        <div className='login-wrapper'>
        <h2>Faça seu registro</h2>
        <form onSubmit={this.handleSubmit} >
            <input
                name='nome'
                onChange={e =>this.handleInputChange('nome', e)}
                value={this.state.name} 
                placeholder='Nome de Usuário'
            />
            <input
                name='email'
                onChange={e =>this.handleInputChange('email', e)}
                value={this.state.email} 
                placeholder='E-mail'
            />
            <input
                name='senha'
                type='password'
                onChange={e =>this.handleInputChange('senha', e)}
                value={this.state.senha} 
                placeholder='Senha'
            />
            <button onChange={this.handleRegistro} type='submit'>Entrar</button>
            <button onChange={this.handleLogin}>Login</button>
            
        </form>
        </div>
    );
  }
}