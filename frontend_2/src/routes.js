import React from 'react';
import { isAuthenticated } from './auth';

import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Main from './pages/main';
import Product from './pages/products';
import Registro from './pages/registro';
import Login from './pages/login';
import Clientes from './pages/clientes';
import Ver_Clientes from './pages/ver_cliente';
import User from './pages/user';


const RotaPrivada = ({ component : Component, ...rest })=>(
    <Route { ...rest} render={ props=>(
        isAuthenticated() ?(
        <Component { ...props } /> 
        ) : (
            <Redirect to={{ pathname: '/login', state: { form:props.location } }}/>
        )
    )}/>
)

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <RotaPrivada  exact path='/main' component={Main}/>
            <RotaPrivada exact path='/product/:id' component={Product}/>
            <RotaPrivada exact path='/clientes/ver/:id' component={Ver_Clientes}/>
            <Route exact path='/login' component={Login}/>
            <RotaPrivada exact path='/clientes' component={Clientes}/>
            <RotaPrivada exact path='/user' component={User}/>
            <Route  exact path='/' component={Registro}/>
        </Switch>
    </BrowserRouter>
);

export default Routes;