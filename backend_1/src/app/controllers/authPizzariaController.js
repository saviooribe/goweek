const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const mailer = require('../../modules/mailer');


const authConfig = require('../../config/auth')

const Pizzaria = require('../models/pizzaria');

const router = express.Router();

function generateToken (params = {}) {
    return jwt.sign(params, authConfig.secret,{
        expiresIn: 86400,
    });
};

router.post('/pizaria_registro', async (req, res) => {
    const { email } = req.body;
    try{
        if(await Pizzaria.findOne({ email }))
        return res.status(400).send({ error: 'Email já registrado' })

        const pizzaria = await Pizzaria.create(req.body);
        
        pizzaria.senha = undefined;

            return res.send({ 
                pizzaria,
                token: generateToken({ id: pizzaria.id}),
            });
            console.log(pizzaria, token)
    } catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Falha ao registra' })
    }
});

router.post('/pizzaria_login', async (req, res) => {
    
    const { email, senha } = req.body
     
        const pizzaria = await Pizzaria.findOne({ email }).select('+senha');

        if(!pizzaria)
            return res.status(400).send({ error: 'Pizzaria não encontrada' });

        if(!await bcrypt.compare(senha, pizzaria.senha))
            return res.status(400).send({ error: 'Senha errada!' });

        pizzaria.senha = undefined; 
        res.send({ 
            pizzaria, 
            token: generateToken({ id: pizzaria.id})
        });
        console.log(req.body.token, pizzaria)
});

router.post('/pizarria_esqueci_minha_senha', async (req, res) => {
    const { email } = req.body;

    try{
        const pizzaria = await Pizzaria.findOne({ email })

        if(!pizzaria)
            return res.status(400).send({ error: 'Email não existe' })

        const token = crypto.randomBytes(20).toString('hex')
        
        const now = new Date();
        now.setHours(now.getHours() + 1 );

        await  Pizzaria.findByIdAndUpdate(pizzaria.id, {
            '$set': {
                senhaResetToken: token,
                senhaResetExpires: now,
            }
        });

        mailer.sendMail({
            to: email,
            from: 'saviojks@gmail.com',
            template: 'auth/esqueci_senha',
            context: { token } 
        }, (err)=>{
            if(err) 
                return res.status(400).send({ error: 'Não foi possivek resetar sua senha...' })
            return send()
        })
    } catch (err){
        return res.status(400).send({ error: 'Erro ao recuperar a senha, tente novamente' })
    }
});

router.post('/pizzaria_reset_sua_senha', async (req, res) => {
    const { email, token, senha } = req.body;
   
    try{
        const pizzaria = await Pizzaria.findOne({ email })
        .select('+senhaResetToken senhaResetExpires');

        if(!pizzaria)
            return res.status(400).send({ error: 'Email não existe' })

        if(token !== pizzaria.senhaResetToken)
            return res.status(400).send({ error: 'token invalido' })
        
        const now = new Date();

        if(now > pizzaria.senhaResetExpires)
        return res.status(400).send({ error: 'token expirado, tente novamente' })

        pizzaria.senha = senha;

        await pizzaria.save();

        res.send();

    } catch (err){
        
        return res.status(400).send({ error: 'Erro ao recuperar a senha, tente novamente' })
    }
});

module.exports = app => app.use('/auth', router);