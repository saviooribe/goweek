const express = require('express');
const authMiddlawere = require('../middleware/auth');
const Pizza = require('../models/pizza');

const router = express.Router();

router.use(authMiddlawere);

// listar
router.get('/pizza', async (req, res) => {
    try{
        const pizza = await Pizza.find();

        return res.send({ pizza })
    }catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Erro ao carregar os pizza' })
    } 

});

//listar 1 / ver
router.get('/ver/:clienteId', async (req, res) => {
    try{
        const cliente = await Pizza.findById(req.params.clienteId);

        return res.send({ cliente })
    }catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Erro ao carregar o projeto' })
    }     
});

//criar
router.post('/pizza', async (req, res) => {
    try{
        const cliente = await Pizza.create(req.body)

        console.log(req.body)
        return res.send({ cliente })
    }catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Erro ao criar o projeto' })
    } 

});

// atualizar
router.put('/:clienteId', async (req, res) => {
    try{
        const { name, endereco} = req.body;

        const cliente = await Cliente.findByIdAndUpdate( req.params.clienteId, {
            name, 
            endereco } , { new: true });

        await cliente.save();

        return res.send({ cliente })
    }catch (err){
        return res.status(400).send({ error: 'Erro ao atualizar o projeto' })
    }     

});

// delete
router.delete('/delete/:clienteId', async (req, res) => {
    try{
        const cliente = await Pizza.findByIdAndRemove(req.params.clienteId)
        console.log(req.params.clienteId)
        return res.send()
    }catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Erro ao deletar o projeto' })
    }     

});

module.exports = app => app.use('/', router);