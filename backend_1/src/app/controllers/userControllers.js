const express = require('express');
const authMiddlawere = require('../middleware/auth');
const User = require('../models/user');

const router = express.Router();

router.use(authMiddlawere);

// listar
router.get('/user', async (req, res) => {
    try{
        const user = await User.find();

        return res.send({ user })
    }catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Erro ao carregar os user' })
    } 

});

//listar 1 / ver
router.get('/ver/:clienteId', async (req, res) => {
    try{
        const cliente = await User.findById(req.params.clienteId);

        return res.send({ cliente })
    }catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Erro ao carregar o projeto' })
    }     
});

//criar
router.post('/user', async (req, res) => {
    try{
        const cliente = await User.create(req.body)

        console.log(req.body)
        return res.send({ cliente })
    }catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Erro ao criar o projeto' })
    } 

});

// atualizar
router.put('/:clienteId', async (req, res) => {
    try{
        const { name, endereco} = req.body;

        const cliente = await Cliente.findByIdAndUpdate( req.params.clienteId, {
            name, 
            endereco } , { new: true });

        await cliente.save();

        return res.send({ cliente })
    }catch (err){
        return res.status(400).send({ error: 'Erro ao atualizar o projeto' })
    }     

});

// delete
router.delete('/delete/:clienteId', async (req, res) => {
    try{
        const cliente = await User.findByIdAndRemove(req.params.clienteId)
        console.log(req.params.clienteId)
        return res.send()
    }catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Erro ao deletar o projeto' })
    }     

});

module.exports = app => app.use('/user', router);