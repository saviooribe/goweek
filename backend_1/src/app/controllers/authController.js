const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const mailer = require('../../modules/mailer');


const authConfig = require('../../config/auth')

const User = require('../models/user');

const router = express.Router();

function generateToken (params = {}) {
    return jwt.sign(params, authConfig.secret,{
        expiresIn: 86400,
    });
};

router.post('/registro', async (req, res) => {
    const { email } = req.body;
    try{
        if(await User.findOne({ email }))
        return res.status(400).send({ error: 'Email já registrado' })

        const user = await User.create(req.body);
        
        user.senha = undefined;

            return res.send({ 
                user,
                token: generateToken({ id: user.id}),
            });
            console.log(user, token)
    } catch (err){
        console.log(err)
        return res.status(400).send({ error: 'Falha ao registra' })
    }
});

router.post('/login', async (req, res) => {
    
    const { email, senha } = req.body
     
        const user = await User.findOne({ email }).select('+senha');

        if(!user)
            return res.status(400).send({ error: 'Usuario não encontrado' });

        if(!await bcrypt.compare(senha, user.senha))
            return res.status(400).send({ error: 'Senha errada!' });

        user.senha = undefined; 
        res.send({ 
            user, 
            token: generateToken({ id: user.id})
        });
        console.log(req.body.token, user)
});

router.post('/esqueci_minha_senha', async (req, res) => {
    const { email } = req.body;

    try{
        const user = await User.findOne({ email })

        if(!user)
            return res.status(400).send({ error: 'Email não existe' })

        const token = crypto.randomBytes(20).toString('hex')
        
        const now = new Date();
        now.setHours(now.getHours() + 1 );

        await  User.findByIdAndUpdate(user.id, {
            '$set': {
                senhaResetToken: token,
                senhaResetExpires: now,
            }
        });

        mailer.sendMail({
            to: email,
            from: 'saviojks@gmail.com',
            template: 'auth/esqueci_senha',
            context: { token } 
        }, (err)=>{
            if(err) 
                return res.status(400).send({ error: 'Não foi possivek resetar sua senha...' })
            return send()
        })
    } catch (err){
        return res.status(400).send({ error: 'Erro ao recuperar a senha, tente novamente' })
    }
});

router.post('/reset_sua_senha', async (req, res) => {
    const { email, token, senha } = req.body;
   
    try{
        const user = await User.findOne({ email })
        .select('+senhaResetToken senhaResetExpires');

        if(!user)
            return res.status(400).send({ error: 'Email não existe' })

        if(token !== user.senhaResetToken)
            return res.status(400).send({ error: 'token invalido' })
        
        const now = new Date();

        if(now > user.senhaResetExpires)
        return res.status(400).send({ error: 'token expirado, tente novamente' })

        user.senha = senha;

        await user.save();

        res.send();

    } catch (err){
        
        return res.status(400).send({ error: 'Erro ao recuperar a senha, tente novamente' })
    }
});

module.exports = app => app.use('/auth', router);