const jwt = require('jsonwebtoken');
authConfig = require('../../config/auth.json');

module.exports = (req, res, next)=>{
    const authHeader = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjNWY1MmZkNGMzMTVkMzNhMjA2MWQ3MiIsImlhdCI6MTU0OTc1MTQzNCwiZXhwIjoxNTQ5ODM3ODM0fQ.yzZhmruuZMXZ4Sk8jbFD4oof9KvZ9NPFJgLAopRknHQ';
    if(!authHeader)
        return res.status(401).send({error: "Token não informado !"});
    
    const parts = authHeader.split(' ');
    
    if(!parts.length === 2 )
        return res.status(401).send({error: "Erro no Token!"});

    const [schema, token] = parts;

    if(!/^Bearer$/i.test(schema))
    return res.status(401).send({error: "Token invalido !"});

    jwt.verify(token, authConfig.secret, (err, decoded) =>{
        if(err) return res.status(401).send({error: "Token invalido!"});
    
    req.userId = decoded.id;

    return next();

    });
};