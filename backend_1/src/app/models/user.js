const mongoose = require('../../database');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    email:{
        type: String,
        unique: true,
        require: true,
        lowercase: true,
    },
    senha:{
        type: String,
        require: true,
        select: false,
    },

    senhaResetToken:{
        type: String,
        select:false
    },
    senhaResetExpires:{
        type: Date,
        select: false,
    },
    endereco:{
        type: String,
        require: true,
    },
    numero:{
        type: Number,
        require: true,
    },
    cidade:{
        type: String,
        require: true,
    },
    estado:{
        type: String,
        require: true,
    },
    celular:{
        type: String,
        require: true,
    },
    cpf:{
        type: String,
        require: true,
    },

    createdAt:{
        type: Date,
        default: Date.now,
    },

});

UserSchema.pre('save', async function(next){
    const hash = await bcrypt.hash(this.senha, 10);
    this.senha = hash;

    next();
})

const User = mongoose.model('User', UserSchema);

module.exports = User;