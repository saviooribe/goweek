const mongoose = require('../../database');
const bcrypt = require('bcryptjs');

const PizzariaSchema = new mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    endereco:{
        type: String,
        require: true,
    },
    numero:{
        type: Number,
        require: true,
    },
    cidade:{
        type: String,
        require: true,
    },
    estado:{
        type: String,
        require: true,
    },
    tipos_entrega:{
        type: String,
        require: true,
    },
    formas_pagamento:{
        type: String,
        require: true,
    },
    valor_entrega:{
        type: String,
        require: true,
    },
    qtd_sabores:{
        type: Number,
        require: true,
    },
    ativo:{
        type: Number,
        require: true,
    },
    cnpj:{
        type: String,
        require: true,
    },
    email:{
        type: String,
        unique: true,
        require: true,
        lowercase: true,
    },
    senha:{
        type: String,
        require: true,
        select: false,
    },

    senhaResetToken:{
        type: String,
        select:false
    },
    senhaResetExpires:{
        type: Date,
        select: false,
    },

    createdAt:{
        type: Date,
        default: Date.now,
    },

});

PizzariaSchema.pre('save', async function(next){
    const hash = await bcrypt.hash(this.senha, 10);
    this.senha = hash;

    next();
})

const Pizzaria = mongoose.model('Pizzaria', PizzariaSchema);

module.exports = Pizzaria;