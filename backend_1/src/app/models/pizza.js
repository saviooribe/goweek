const mongoose = require('../../database');

const PizzaSchema = new mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    pizzaria:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pizzaria',
        require: true,
    },
    ingredientes:{
        type: String,
        require: true,
    },
    tempo_producao:{
        type: String,
        require: true,
    },
    preco_p:{
        type: String,
    },
    preco_m:{
        type: String,
    },
    preco_g:{
        type: String,
    },
    preco_gg:{
        type: String,
    },
    borda_recheada:{
        type: String,
        require: true,
    },
    preco_borda:{
        type: String,
    },
    sabor_borda:{
        type: String,
    },
    img:{
        type: String,
    },    
    createdAt:{
        type: Date,
        default: Date.now,
    },

});

const Pizza = mongoose.model('Pizza', PizzaSchema);

module.exports = Pizza;