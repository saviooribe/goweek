const mongoose = require('mongoose');

mongoose.connect('mongodb://saviojks:ramona.jks13@ds129085.mlab.com:29085/pizzaria', 
    {
    useNewUrlParser: true,
    });
mongoose.set('useCreateIndex', true)
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);

module.exports = mongoose;